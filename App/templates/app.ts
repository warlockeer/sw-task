/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

class Validator {
    /**
     *
     * @param event
     * @param formElement
     * @param dataMessageAttribute
     */
    public constructor(event: string, formElement: string, dataMessageAttribute: string) {
        this.event = event;
        this.setFormElement(formElement);
        this.setDataMessageAttribute(dataMessageAttribute);
    }

    /**
     *
     * @param elementName
     * @private
     */
    private setFormElement(elementName: string) {
        if ($(elementName).length === 0) {
            this.event.preventDefault();
            throw `Form ${elementName} doesn't exist`;
        }

        this.formElement = elementName;
    }

    /**
     *
     * @param attributeName
     * @private
     */
    private setDataMessageAttribute(attributeName: string) {
        this.dataMessageAttribute = attributeName;
    }

    public clearMessages() {
        $('input').removeClass('add-form__input--error');
        $(`[${this.dataMessageAttribute}]`).text('');
        $(`[${this.dataMessageAttribute}]`).css('display', 'none');
    }

    /**
     *
     * @param input
     * @param code
     * @param message
     */
    public isRequired(input: string, code: string, message: string) {
        let value = $(`${this.formElement} [name=${input}]`).val();
        if (value.length === 0) {
            this.event.preventDefault();
            $(`${this.formElement} [name=${input}]`).addClass('add-form__input--error');
            $(`[${this.dataMessageAttribute}=${code}]`).text(message);
            $(`[${this.dataMessageAttribute}=${code}]`).css('display', 'block');

        }
    }
}

$(window).ready(function () {
    $(".add-form").submit(function (event) {
        const validator = new Validator(event, ".add-form", "data-flash-message");
        const productType = $('#productType').find(":selected").text();
        validator.clearMessages();
        validator.isRequired("sku", "sku_is_required", "Sku is required!");
        validator.isRequired("name", "name_is_required", "Name is required!");
        validator.isRequired("price", "price_is_required", "Price is required!");
        if (productType === 'Book') {
            validator.isRequired("weight", "weight_is_required", "Weight is required!");
        } else if (productType === 'DVD') {
            validator.isRequired("size", "size_is_required", "Size is required!");
        } else if (productType === 'Furniture') {
            validator.isRequired("height", "height_is_required", "Height is required!");
            validator.isRequired("length", "length_is_required", "Length is required!");
            validator.isRequired("width", "width_is_required", "Width is required!");
        }
    });
});