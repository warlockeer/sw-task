<?php

namespace App;

class Config
{

    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'products';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'root';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = '12345678A';

    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = TRUE;

    /**
     * Basic View engine templates path relative to Core directory
     * @var string
     */
    const VIEW_PATH = "../App/templates/";
}
