<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace App;

abstract class ProductService
{
    /**
     * @return string
     */
    static public function getType(): string
    {
        $class = explode('\\',static::class);
        $class = explode('Service', end($class));

        return strtolower($class[0]);
    }

    /**
     * @param array $params
     * @return object
     */
    abstract function addProduct(array $params): object;

    /**
     * @return array
     */
    abstract function getAllProducts(): array;

    /**
     * @param string $params
     * @return void
     */
    abstract function deleteProduct(string $params): void;
}