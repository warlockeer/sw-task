<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

use App\Config;
use App\Products\Book\BookRepository;
use App\Products\Book\BookRepositoryMysql;
use App\Products\Dvd\DvdRepository;
use App\Products\Dvd\DvdRepositoryMysql;
use App\Products\Furniture\FurnitureRepository;
use App\Products\Furniture\FurnitureRepositoryMysql;
use Core\TemplateEngine;
use Core\TwigTemplateEngine;

/**
 * Composer
 */
require '../vendor/autoload.php';

/**
 * Twig
 */
$loader = new Twig_Loader_Filesystem('../App/templates');
$twig = new Twig_Environment($loader);
$templateEngine = new TwigTemplateEngine($twig);

/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

/**
 * Database connection
 */

$dsn = 'mysql:host=' . Config::DB_HOST . ';dbname=' . Config::DB_NAME . ';charset=utf8';
$pdo = new PDO($dsn, Config::DB_USER, Config::DB_PASSWORD);
// Throw an Exception when an error occurs
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

/**
 * Session start
 */
session_start();

/**
 * Di container building
 */
$builder = new DI\ContainerBuilder();
$builder->addDefinitions([
    PDO::class => function () use ($pdo) {
        return $pdo;
    },
    BookRepository::class => DI\get(BookRepositoryMysql::class),
    DvdRepository::class => DI\get(DvdRepositoryMysql::class),
    FurnitureRepository::class => DI\get(FurnitureRepositoryMysql::class),
    TwigTemplateEngine::class => function () use ($templateEngine) {
        return $templateEngine;
    },
    TemplateEngine::class => DI\get(TwigTemplateEngine::class)]);
$container = $builder->build();

/**
 * Routing
 */
$router = new Core\Router($container);

// Add the routes
$router->add('', ['controller' => 'products', 'action' => 'index']);
$router->add('{controller}/{action}');
$router->add('{controller}/{id:\d+}/{action}');

$router->dispatch($_SERVER['QUERY_STRING']);