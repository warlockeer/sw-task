<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace App\Controllers;

use App\Products\Book\BookService;
use App\Products\Components\Validator;
use App\Products\Dvd\DvdService;
use App\Products\Furniture\FurnitureService;
use Core\Controller;
use Exception;


class ProductsController extends Controller
{
    /**
     * Show the index page
     *
     * @return string
     * @throws Exception
     */
    public function indexAction(): string
    {
        $factory = $this->serviceFactory;

        $bookService = $factory->getService(BookService::getType());
        $dvdService = $factory->getService(DvdService::getType());
        $furnitureService = $factory->getService(FurnitureService::getType());

        $booksTable = $bookService->getAllProducts();
        $dvdsTable = $dvdService->getAllProducts();
        $furnitureTable = $furnitureService->getAllProducts();

        $allProducts = [...$booksTable, ...$dvdsTable, ...$furnitureTable];
        usort($allProducts, function ($a, $b) {
            return $b->getCreatedAt() <=> $a->getCreatedAt();
        });

        return $this->render('products/index.html.twig', ['allProducts' => $allProducts]);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function saveProductAction()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            try {
                $factory = $this->serviceFactory;

                $validator = new Validator($_POST);
                $validator->isRequired("sku", "sku_is_required", "Sku can not be empty.");
                $validator->isRequired("name", "name_is_required", "Name can not be empty.");
                $validator->isRequired("price", "price_is_required", "Price can not be empty.");

                switch ($_POST['type']) {
                    case('book'):
                        $validator->isRequired("weight", "weight_is_required", "Weight can not be empty.");
                        break;
                    case('dvd'):
                        $validator->isRequired("size", "size_is_required", "Size can not be empty.");
                        break;
                    case('furniture'):
                        $validator->isRequired("height", "height_is_required", "Height can not be empty.");
                        $validator->isRequired("length", "length_is_required", "Length can not be empty.");
                        $validator->isRequired("width", "width_is_required", "Width can not be empty.");
                        break;
                    default:
                        throw new Exception("No such product type");
                }

                if ($validator->isFailed()) {
                    return $this->render('products/addproduct.html.twig', ['validator' => $validator]);
                }

                $service = $factory->getService($_POST['type']);
                $service->addProduct($_POST);
                $this->redirect('/');
            } catch (Exception $e) {
                throw new Exception("Can't add product", 0, $e);
            }
        }
    }

    /**
     * Show the page to add a product
     * @return string
     * @throws Exception
     */
    public function addProductAction(): string
    {
        return $this->render('products/addproduct.html.twig', []);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function deleteAction(): void
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST)) {
            $factory = $this->serviceFactory;
            if (!empty($_POST['book'])) {
                $book = implode(', ', $_POST['book']);
                $bookService = $factory->getService(BookService::getType());
                $bookService->deleteProduct($book);
            }

            if (!empty($_POST['dvd'])) {
                $dvd = implode(', ', $_POST['dvd']);
                $dvdService = $factory->getService(DvdService::getType());
                $dvdService->deleteProduct($dvd);
            }

            if (!empty($_POST['furniture'])) {
                $furniture = implode(', ', $_POST['furniture']);
                $furnitureService = $factory->getService(FurnitureService::getType());
                $furnitureService->deleteProduct($furniture);
            }
        }

        $this->redirect('/');
    }
}
