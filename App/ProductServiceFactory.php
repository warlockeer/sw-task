<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace App;

use App\Products\Book\BookService;
use App\Products\Dvd\DvdService;
use App\Products\Furniture\FurnitureService;
use Exception;

class ProductServiceFactory
{
    /**
     * @var BookService
     */
    private BookService $bookService;
    /**
     * @var DvdService
     */
    private DvdService $dvdService;
    /**
     * @var FurnitureService
     */
    private FurnitureService $furnitureService;

    /**
     * @param BookService $bookService
     * @param DvdService $dvdService
     * @param FurnitureService $furnitureService
     */
    public function __construct(BookService $bookService, DvdService $dvdService, FurnitureService $furnitureService)
    {
        $this->bookService = $bookService;
        $this->dvdService = $dvdService;
        $this->furnitureService = $furnitureService;
    }

    /**
     * @param string $type
     * @return BookService|DvdService|FurnitureService
     * @throws Exception
     */
    public function getService(string $type): object
    {
        switch ($type) {
            case BookService::getType():
                return $this->bookService;
            case DvdService::getType():
                return $this->dvdService;
            case FurnitureService::getType():
                return $this->furnitureService;
            default:
                throw new Exception('No such product type.');
        }
    }
}
