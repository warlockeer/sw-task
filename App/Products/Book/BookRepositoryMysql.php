<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace App\Products\Book;

use Exception;
use PDO;

class BookRepositoryMysql implements BookRepository
{
    /**
     * @var PDO
     */
    public PDO $pdo;

    /**
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param Book $book
     * @return Book
     * @throws Exception
     */
    public function addBook(Book $book): Book
    {
        try {
            $sql = "INSERT INTO books(sku, name, price, type, weight) VALUES (:sku, :name, :price, :type, :weight)";
            $statement = $this->pdo->prepare($sql);
            $statement->execute([
                ':sku' => $book->getSku(),
                ':name' => $book->getName(),
                ':price' => $book->getPrice(),
                ':type' => $book->getType(),
                ':weight' => $book->getWeight()
            ]);
            return $book;
        } catch (Exception $e) {
            throw new Exception('Cant save to mysql repository', 0, $e);
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getAllBooks(): array
    {
        try {
            $statement = $this->pdo->query('SELECT id, sku, name, price, type, weight, created_at FROM books ORDER BY created_at DESC');
            $results = $statement->fetchAll(PDO::FETCH_ASSOC);
            $bookCollection = array_map(function ($book) {
                return Book::fromDatabase($book['sku'], $book['name'], $book['price'], $book['type'], $book['weight'], $book['id'], $book['created_at']);
            }, $results);
            return $bookCollection;
        } catch (Exception $e) {
            throw new Exception('Cant get books from mysql repository', 0, $e);
        }
    }

    /**
     * @param string $sku
     * @return bool
     */
    public function exists(string $sku) :bool
    {
        $statement = $this->pdo->prepare('SELECT sku FROM books WHERE sku=:sku');
        $statement->execute([':sku' => $sku]);

        return !empty($statement->fetchAll());
    }

    /**
     * @param string $id
     * @throws Exception
     */
    public function deleteBook(string $id) :void
    {
        try {
            $sql = "DELETE FROM books WHERE id IN ($id)";
            $statement = $this->pdo->prepare($sql);
            $statement->execute();
        } catch (Exception $e) {
            throw new Exception('Cant delete book from mysql repository', 0, $e);
        }
    }
}
