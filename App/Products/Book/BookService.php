<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace App\Products\Book;

use App\ProductService;
use Exception;

class BookService extends ProductService
{
    /**
     * @var BookRepository
     */
    private BookRepository $bookRepository;

    /**
     * @param BookRepository $bookRepository
     */
    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    /**
     * @param array $params
     * @return object
     * @throws Exception
     */
    public function addProduct(array $params): Book
    {
        $newBook = new Book($params['sku'], $params['name'], $params['price'], $params['type'], $params['weight']);
        $savedBook = $this->bookRepository->addBook($newBook);

        return $savedBook;
    }

    /**
     * @return array
     */
    public function getAllProducts(): array
    {
        $books = $this->bookRepository->getAllBooks();

        return $books;
    }

    /**
     * @param string $bookId
     * @return void
     */
    public function deleteProduct(string $bookId): void
    {
        $this->bookRepository->deleteBook($bookId);
    }
}