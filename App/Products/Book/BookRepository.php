<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace App\Products\Book;

interface BookRepository
{
    /**
     * @param Book $book
     * @return Book
     */
    public function addBook(Book $book): Book;

    /**
     * @return array
     */
    public function getAllBooks(): array;


    /**
     * @param string $id
     * @return void
     */
    public function deleteBook(string $id): void;
}