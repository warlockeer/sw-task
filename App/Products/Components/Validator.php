<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace App\Products\Components;

class Validator
{
    /**
     * Validator session
     */
    const VALIDATOR_SESSION = "form_validation";

    /**
     * @var array
     */
    private array $data;

    /**
     * Validator constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param string $code
     * @return string
     */
    public static function flashMessage(string $code)
    {
        if (!empty($_SESSION[self::VALIDATOR_SESSION][$code])) {
            $validationMessage = $_SESSION[self::VALIDATOR_SESSION][$code];
            unset($_SESSION[self::VALIDATOR_SESSION][$code]);
            return $validationMessage;
        }
    }

    /**
     * @param string $inputName
     * @param string $code
     * @param string $message
     */
    public function isRequired(string $inputName, string $code, string $message)
    {
        if (strlen($this->data[$inputName]) === 0) {
            $_SESSION[self::VALIDATOR_SESSION][$code] = $message;
        }
    }

    /**
     * @param string $inputName
     * @param string $code
     * @param string $message
     */
    public function isAlphaNumeric(string $inputName, string $code, string $message)
    {
        if (!preg_match("#\w+#", $this->data[$inputName])) {
            $_SESSION[self::VALIDATOR_SESSION][$code] = $message;
        }
    }

    /**
     * @return bool
     */
    public function isFailed(): bool
    {
        return !empty($_SESSION[self::VALIDATOR_SESSION]);
    }
}