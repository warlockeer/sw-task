<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace App\Products;

use Exception;

abstract class Product
{
    /**
     * @var int
     */
    private int $id;
    /**
     * @var string
     */
    private string $sku;
    /**
     * @var string
     */
    private string $name;
    /**
     * @var float
     */
    private float $price;
    /**
     * @var string
     */
    private string $type;
    /**
     * @var string
     */
    private string $created_at;

    /**
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param string $type
     * @throws Exception
     */
    public function __construct(string $sku, string $name, float $price, string $type)
    {
        $this->setSku($sku);
        $this->setName($name);
        $this->setPrice($price);
        $this->setType($type);
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     * @throws Exception
     */
    public function setSku(string $sku): void
    {
        if (strlen($sku) === 0) {
            throw new Exception("Invalid sku value.");
        }

        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @throws Exception
     */
    public function setName(string $name): void
    {
        if (strlen($name) === 0) {
            throw new Exception("Invalid name value.");
        }

        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @throws Exception
     */
    public function setPrice(float $price): void
    {
        if (strlen($price) === 0) {
            throw new Exception("Invalid price value.");
        }

        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @throws Exception
     */
    public function setType(string $type): void
    {
        if (strlen($type) === 0) {
            throw new Exception("Invalid type value.");
        }

        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt(string $created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}