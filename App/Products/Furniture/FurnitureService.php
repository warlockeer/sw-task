<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace App\Products\Furniture;

use App\ProductService;
use Exception;

class FurnitureService extends ProductService
{
    /**
     * @var FurnitureRepository
     */
    private FurnitureRepository $furnitureRepository;

    /**
     * @param FurnitureRepository $furnitureRepository
     */
    public function __construct(FurnitureRepository $furnitureRepository)
    {
        $this->furnitureRepository = $furnitureRepository;
    }

    /**
     * @param array $params
     * @return Furniture
     * @throws Exception
     */
    public function addProduct(array $params): Furniture
    {
        $newFurniture = new Furniture($params['sku'], $params['name'], $params['price'], $params['type'], $params['height'], $params['length'], $params['width']);
        $savedFurniture = $this->furnitureRepository->addFurniture($newFurniture);

        return $savedFurniture;
    }

    /**
     * @return array
     */
    public function getAllProducts(): array
    {
        $furniture = $this->furnitureRepository->getAllFurniture();

        return $furniture;
    }

    /**
     * @param string $furnitureId
     */
    public function deleteProduct(string $furnitureId): void
    {
        $this->furnitureRepository->deleteFurniture($furnitureId);
    }

}