<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace App\Products\Furniture;

interface FurnitureRepository
{
    /**
     * @param Furniture $furniture
     * @return Furniture
     */
    public function addFurniture(Furniture $furniture): Furniture;

    /**
     * @return array
     */
    public function getAllFurniture(): array;

    /**
     * @param string $id
     * @return void
     */
    public function deleteFurniture(string $id): void;
}