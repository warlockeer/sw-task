<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace App\Products\Dvd;

use App\ProductService;
use Exception;

class DvdService extends ProductService
{
    /**
     * @var DvdRepository
     */
    private DvdRepository $dvdRepository;

    /**
     * @param DvdRepository $dvdRepository
     */
    public function __construct(DvdRepository $dvdRepository)
    {
        $this->dvdRepository = $dvdRepository;
    }

    /**
     * @param array $params
     * @return Dvd
     * @throws Exception
     */
    public function addProduct(array $params): Dvd
    {
        $newDvd = new Dvd($params['sku'], $params['name'], $params['price'], $params['type'], $params['size']);
        $savedDvd = $this->dvdRepository->addDvd($newDvd);

        return $savedDvd;
    }

    /**
     * @return array
     */
    public function getAllProducts(): array
    {
        $dvds = $this->dvdRepository->getAllDvds();

        return $dvds;
    }

    /**
     * @param string $dvdId
     * @return void
     */
    public function deleteProduct(string $dvdId): void
    {
        $this->dvdRepository->deleteDvd($dvdId);
    }
}