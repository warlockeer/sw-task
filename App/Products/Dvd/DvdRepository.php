<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace App\Products\Dvd;

interface DvdRepository
{
    /**
     * @param Dvd $dvd
     * @return Dvd
     */
    public function addDvd(Dvd $dvd): Dvd;

    /**
     * @return array
     */
    public function getAllDvds(): array;

    /**
     * @param string $id
     * @return void
     */
    public function deleteDvd(string $id): void;
}