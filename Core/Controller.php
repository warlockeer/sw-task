<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace Core;

use App\ProductServiceFactory;
use Exception;

abstract class Controller
{
    /**
     * @var ProductServiceFactory
     */
    protected ProductServiceFactory $serviceFactory;
    /**
     * @var TemplateEngine
     */
    protected TemplateEngine $templateEngine;

    /**
     * Class constructor
     *
     * @param ProductServiceFactory $serviceFactory
     * @param TemplateEngine $templateEngine
     */
    public function __construct(ProductServiceFactory $serviceFactory,TemplateEngine $templateEngine)
    {
        $this->serviceFactory = $serviceFactory;
        $this->templateEngine = $templateEngine;
    }


    /**
     * Magic method called when a non-existent or inaccessible method is
     * called on an object of this class. Used to execute before and after
     * filter methods on action methods. Action methods need to be named
     * with an "Action" suffix, e.g. indexAction, showAction etc.
     *
     * @param string $name Method name
     * @param array $args Arguments passed to the method
     *
     * @return void
     * @throws Exception
     */
    public function __call(string $name, array $args)
    {
        $method = $name . 'Action';

        if (method_exists($this, $method)) {
            echo call_user_func_array([$this, $method], $args);
        } else {
            throw new Exception("Method $method not found in controller " . get_class($this));
        }
    }

    /**
     * @param string $parameter
     * @return void
     */
    public function redirect(string $parameter)
    {
        header("Location: " . $parameter);
    }

    /**
     * @param string $template
     * @param array $args
     * @return string
     */
    public function render(string $template, array $args = []): string
    {
        return $this->templateEngine->render($template, $args);
    }
}
