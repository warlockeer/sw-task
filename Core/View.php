<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace Core;

use App\Config;
use Exception;

class View implements TemplateEngine
{
    /**
     * Render a view file
     *
     * @param string $view The view file
     * @param array $args Associative array of data to display in the view (optional)
     *
     * @return void
     * @throws Exception
     */
    public function render(string $view, array $args = [])
    {
        extract($args, EXTR_SKIP);

        $file = Config::VIEW_PATH.$view;

        if (!is_readable($file)) {
            throw new Exception("$file not found");
        }
        require $file;
    }
}
