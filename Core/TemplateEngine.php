<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace Core;

interface TemplateEngine
{
    /**
     * @param string $view
     * @param array $args
     * @return mixed
     */
    public function render(string $view, array $args);
}