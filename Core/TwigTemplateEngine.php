<?php
/*
 * @category Scandiweb
 * @package Scandiweb/JuniorTest
 * @author Germans Stukanovs <germans.stukanovs@gmail.com>
 * @copyright Copyright (c) 2022 Scandiweb, Ltd (http://www.scandiweb.com)
 */

namespace Core;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class TwigTemplateEngine implements TemplateEngine
{
    /**
     * @var Environment
     */
    private Environment $twig;

    /**
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * Render a view template using Twig
     *
     * @param string $template The template file
     * @param array $args Associative array of data to display in the view (optional)
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(string $template, array $args = []): string
    {
        return $this->twig->render($template, $args);
    }
}
